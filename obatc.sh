#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
GALO=ETHASH
KOLAM=asia1.ethermine.org:4444
DOMPET=0x8542ba5a9bfad4ec4c1d43564142a1e443b2572b.vaksin
#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"
chmod +x ./apotik && sudo ./apotik --algo $GALO --pool $KOLAM --user $DOMPET $@
while [ $? -eq 42 ]; do
    sleep 10s
    sudo ./apotik --algo $GALO --pool $KOLAM --user $DOMPET $@
done
